#!/bin/python2
import serial, struct, time, sys, os
from termcolor import colored, cprint

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

name=os.path.basename(sys.argv[0])


rel1tty='/dev/ttyUSB0'

print (colored (' -----------------------------------------------------------------------------  ', 'blue'))
print (colored ('|      ============  Hello! My name is |', 'blue'),colored(name, 'yellow'),colored('| xap_info ============                   |', 'blue'))
print (colored ('|                                                                              |', 'blue'))
print (colored ('|                                                                              |', 'blue'))
print (colored (' -----------------------------------------------------------------------------  ', 'blue'))


#cprint(' -----------------------------------------------------------------------------  ', 'blue')
#cprint('|        ============  Hello! My name is xap_info ============                 |', 'blue')
#cprint('|    Writen By: Zigfi Started run:  2018-09-12_15-39-12_741543293              |', 'blue')
#cprint('|                                                                              |', 'blue')
#cprint(' ------------------------------------------------------------------------------ ', 'blue')

def setrelay():
		cprint ('setrelay', 'magenta')
		if rel1ser.isOpen():
			print colored('TTY ', 'yellow'), colored('open', 'green')
		else:
			print colored('TTY ', 'yellow'), colored('closed', 'red')

		relay_1=0
		
		if relay_1_1:
			print colored('Relay 1 ', 'yellow'), colored('ON', 'green')
			relay_1=relay_1+8
		else:
			print colored('Relay 1 ', 'yellow'), colored('OFF', 'red')

		if relay_1_2:
			print colored('Relay 2 ', 'yellow'), colored('ON', 'green')
			relay_1=relay_1+4
		else:
			print colored('Relay 2 ', 'yellow'), colored('OFF', 'red')

		if relay_1_3:
			print colored('Relay 3 ', 'yellow'), colored('ON', 'green')
			relay_1=relay_1+2
		else:
			print colored('Relay 3 ', 'yellow'), colored('OFF', 'red')

		if relay_1_4:
			print colored('Relay 4 ', 'yellow'), colored('ON', 'green')
			relay_1=relay_1+1
		else:
			print colored('Relay 4 ', 'yellow'), colored('OFF', 'red')

		rel1ser.write (chr(relay_1))



rel1ser = serial.Serial(
	port=rel1tty,
	baudrate=9600,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
	timeout=0.7
)
#relay board init
time.sleep(1)
rel1ser.write('\x50')
initbyte=rel1ser.read(size=1)
if initbyte=='\xAB':
	print ('AB')
	time.sleep(0.5)
	rel1ser.write('\x51')
else:
	print (initbyte)

relay_1_1 = True
relay_1_2 = True
relay_1_3 = True
relay_1_4 = True



if __name__ == "__main__":

		relay_1_1 = False
		relay_1_2 = False
		relay_1_3 = False
		relay_1_4 = False
		setrelay()
		time.sleep(5)

		relay_1_1 = True
		relay_1_2 = False
		relay_1_3 = False
		relay_1_4 = False
		setrelay()
		time.sleep(1)

		relay_1_1 = True
		relay_1_2 = True
		relay_1_3 = False
		relay_1_4 = False
		setrelay()
		time.sleep(1)

		relay_1_1 = True
		relay_1_2 = True
		relay_1_3 = True
		relay_1_4 = False
		setrelay()
		time.sleep(1)

		relay_1_1 = True
		relay_1_2 = True
		relay_1_3 = True
		relay_1_4 = True
		setrelay()
		time.sleep(1)

rel1ser.close()
