#!/bin/python2
import os, subprocess, time, sys,shlex

from urlparse import urlparse

import simplejson as json

def jread(dfile):
  try:
    jfile = open(dfile,'r+')
    jdata = jfile.read();
    jfile.close()
    return json.loads(jdata)
  except IOError:
    return None
    
def jwrite(jdata,dfile):
  jfile = open(dfile,'w+')
  jfile.write(json.dumps(jdata, sort_keys=True, indent=2 * ' '))
  jfile.close()

def getip():
  command='ip -o -4 addr show'
  args=shlex.split(command)
  proc=subprocess.Popen(args, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
  out,err=proc.communicate()
  outlns=out.split('\n')
  iplist=[]
  for line in outlns:
    if len(line)>5:
      ln=line.split()
      ip=ln[3].split('/')
      iplist.append({'name':ln[1],'ip':ip[0],'mask':ip[1]})
  return iplist

def homeip():
  test=False
  iplist=getip()
  for ipline in iplist:
    ww,xx,yy,zz,=ipline['ip'].split('.')
    if ww=='10' and xx=='10' and yy=='27':
      test=True
  return test


def xterm(command,xti='subcmd',h=65,w=5,x=0,y=300,col=0):
 xge=str(h)+'x'+str(w)+'+'+str(x)+'+'+str(y)                                               #size & position 
 xex='/bin/xterm'                                                                          #xterm executable
 xfn='6x10'
 #if col==0: 
 xbg='rgb:00/00/00'; xfg='rgb:FF/FF/FF'                                                                                #font
 if col==1: xbg='rgb:36/23/0b'; xfg='rgb:90/90/90'
 if col==2: xbg='rgb:9d/12/00'; xfg='rgb:00/00/00'
 if col==3: xbg='rgb:FF/99/00'; xfg='rgb:00/00/00'
 if col==4: xbg='rgb:FF/FF/00'; xfg='rgb:00/00/00'
 if col==5: xbg='rgb:00/99/00'; xfg='rgb:00/00/00'
 if col==6: xbg='rgb:00/29/A3'; xfg='rgb:00/00/00'
 if col==7: xbg='rgb:7A/29/7A'; xfg='rgb:00/00/00'  
 if col==8: xbg='rgb:80/80/80'; xfg='rgb:00/00/00'
 if col==9: xbg='rgb:FF/FF/FF'; xfg='rgb:20/20/20'

 xpar=[xex, '-fn',xfn, '-fg',xfg, '-bg',xbg, '-geometry',xge, '-title',xti, '-e',command]  #command assembly
 proc=subprocess.Popen(xpar)                                                                    #execute it
 #subcmd = subprocess.Popen(xpar,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
 #output,errors = subcmd.communicate()
 #print str(output)
 return proc

class XTERM:
  """ class for managing xterm windows """
  def __init__(self,h=65,w=5,x=0,y=300,xbg='000000', xfg='999999',xti='',xfn='6x10',xex='/bin/xterm'):          #Predefined window size, colors and xterm exec
    self.h=h
    self.w=w
    self.x=x
    self.y=y
    self.bg=xbg
    self.fg=xfg
    self.ti=xti
    self.fn=xfn
    self.ex=xex
    self.process=None                                                                                            #subpricess object
    self.subcmd='sh'                                                                                             #test conmand to call
    self.xpar=None                                                                                               #assembled parameters
    self.enabled=True                                                                                            #enabled.swirch

  def open(self,subcmd=None):
    if subcmd==None: subcmd=self.subcmd
    self.subcmd=subcmd

    xbg='rgb:'+self.bg[:2]+'/'+self.bg[2:][:2]+'/'+self.bg[4:][:2]                                               #backgrounc color 
    xfg='rgb:'+self.fg[:2]+'/'+self.fg[2:][:2]+'/'+self.fg[4:][:2]                                               #foregrounf color
    xge=str(self.h)+'x'+str(self.w)+'+'+str(self.x)+'+'+str(self.y)                                              #size & position 

    xpar=[self.ex, '-fn',self.fn, '-fg',xfg, '-bg',xbg, '-geometry',xge, '-title',self.ti, '-e',subcmd]          #command assembly
    self.process=subprocess.Popen(xpar)
    self.xpar=xpar


  def close(self):
    self.process.terminate()
    del self.process

  def kill(self):
    self.process.kill()
    del self.process

  def isInRun(self):
    test=self.process.poll()
    return (test==None)                                                                                           # warunek co bylby w if jest wartoscia boolean!

class XTERManager:
  """Managing a xterm sessions"""
  def __init__(self,initW=65,initH=5,stepX=398,stepY=78,numY=9,initX=0,initY=10,title='Cmd '):

    self.commands=[]

    self.colorSet=[
    ['000000','FFFFFF'],
    ['36230b','909090'],
    ['9d1200','000000'],
    ['FF9900','000000'],
    ['FFFF00','000000'],
    ['009900','000000'],
    ['0029A3','000000'],
    ['7A297A','000000'],
    ['808080','000000'],
    ['FFFFFF','202020']
    ]
    self.initW=initW             #number chars in window
    self.initH=initH

    self.stepX=stepX
    self.stepY=stepY
    self.numY=numY               #number of rows

    self.processes=[]
    self.title=title

    self.stateX=initX
    self.stateY=initY
    self.stateC=1
    self.index=1

    self.enabled=True


  def openadd(self, command):
    if self.enabled:
      x=self.stateX
      y=self.stateY
      i=self.index
      c=self.stateC                                         #color

      bgColor=self.colorSet[c][0]
      fgColor=self.colorSet[c][1]
      title=self.title+str(i)

      xterm=XTERM(self.initW,self.initH,x,y,bgColor,fgColor,title)
      xterm.open(command)
      self.processes.append(xterm)      
      i+=1
      y=y+self.stepY

      c+=1
      if c>9: c=0


      self.stateX=x
      self.stateY=y
      self.stateC=c
      self.index=i
      return(xterm)

  def openall(self,commands=[]):
    if self.enabled:
      for command in commands:
        self.openadd(command)

  def opentest(self,command,sleep=1,xtry=10):
    if self.enabled:
      prc=self.openadd(command)
      time.sleep(sleep)
      for j in range(1,xtry+1):
        if not prc.isInRun():
          prc.open()
          print j
          time.sleep(sleep)
          if j>=xtry:
            print('maximum reached!') 
            self.enabled=False

  def openwait(self,command):
    if self.enabled:
      xterm=self.openadd(command)
      while xterm.isInRun():
        time.sleep(0.5)

  def closeall(self):
    if self.enabled:
      for xterm in self.processes:
        if xterm.isInRun(): 
          xterm.close()

  def killall(self):
    for xterm in self.processes:
      xterm.kill()      

  def reopenall(self):
    if self.enabled:
      i=0
      for xterm in self.processes:
        i+=1
        if not xterm.isInRun(): 
          xterm.open()
          print 're-opened window', i




def config(cfg_file="zigfi.cfg"):
  datafile = open (cfg_file,'r+')
  configdta=datafile.readlines()
  datafile.close()
  section_name=''
  host=''
  sectionlist=[]

  redirport0 = ''
  vnc_port = ''
  ssh_user = ''
  ssh_pass = ''
  ssh_port = ''

  rdp_user = ''
  rdp_pass = ''
  rdp_port = ''

  for line in configdta:
    if len(line)>1:
      if line[0]!='#':               										# pass coment lines.

        if line[0]=='[':            										# assuming [ is a begin of section name
          section_name=line.strip('[] \r\n\t')

          try:
            sectionlist.append(temp_dic)        				# Add only filled dic to the list. 
          except NameError:
            pass

          temp_dic={}                               		# create dic
          temp_dic.clear()                            	# make sure if it is empty
          temp_dic.update({'name':section_name})      	# add section name to it.
          stype = ''

        if section_name=='comment': section_name=''     #section comment will not be processed.

        if len(section_name)>1 and  line[0]!='[' :      #we don't want to process empty lines and section names, because they was processed before.

          if len(line.split('='))>1:
            ident=line.split('=',1)[0].strip(' \r\n\t')
            idata=line.split('=',1)[1].strip('\r\n')

            if section_name=='defaults':        				#for section default
              if ident=='redirport0': redirport0=idata.strip(' \t')
              if ident=='vnc'       : vnc=idata.strip(' \t')

              if ident=='ssh_user'  : ssh_user=idata.strip(' \t')
              if ident=='ssh_pass'  : ssh_pass=idata.strip(' \t')
              if ident=='ssh_port'  : ssh_port=idata.strip(' \t')

              if ident=='rdp_user'  : rdp_user=idata.strip(' \t')
              if ident=='rdp_pass'  : rdp_pass=idata.strip(' \t')
              if ident=='rdp_port'  : rdp_port=idata.strip(' \t')

              if ident=='vnc_port'  : vnc_port=idata.strip(' \t')

            temp_dic.update({ident:idata})
            if ident=='type': stype=idata

																												# here should be full set of sections in sectionlist.
  hostslist = []                                 				#list of all hosts. 
  gatelist  = []
  netlist   = []
  
  for section in sectionlist:
    try: 
      stype=section['type'].strip(' \t')
      sname=section['name'].strip(' \t')
      if stype=='network':
        netlist.append(sname)

        for key,value in section.items():
            if key.split(' ')[0] == 'host':
                hostname=key.split(' ')[1]
                url=urlparse(value.strip(' \t')) 

                hostdic={
                  'host'      : hostname, 
                  'net'       : sname, 
                  'protocol'  : url.scheme,
                  'ip'        : url.hostname,
                  'port'      : url.port,
                  'username'  : url.username,
                  'password'  : url.password
                  }
                
                if hostdic['protocol'] == 'ssh':
                	if hostdic['username'] == None: 
                		hostdic['username'] = ssh_user
                		if hostdic['password'] == None: 
                			hostdic['password'] = ssh_pass
                	if hostdic['port'] == None: hostdic['port'] = ssh_port

                if hostdic['protocol'] == 'rdp':
                	if hostdic['username'] == None: 
                		hostdic['username'] = rdp_user
                		if hostdic['password'] == None: 
                			hostdic['password'] = rdp_pass
                	if hostdic['port'] == None: hostdic['port'] = rdp_port
                if hostdic['protocol'] == 'vnc':
                	if hostdic['port'] == None: hostdic['port'] = vnc_port

               	hostslist.append(hostdic)

      if stype=='gate':
        subtype=True
        try:
          if section['subtype'].strip(' ')=='viaonly': subtype=False
        except KeyError: 
          subtype=True            #If true, make menu for it.

        try:
          host=urlparse(section['host'].strip(' \t'))
        except KeyError: 
          print 'ERROR! Missing host declaration in '+sname
  
        try:
          access=section['access'].strip(' \t').split(',')

          gatedic={
            'host'      : sname, 
            'access'    : access, 
            'subtype'   : subtype,
            'ip'        : host.hostname,
            'port'      : host.port,
            'username'  : host.username,
            'password'  : host.password
            }

          if gatedic['username'] == None:	gatedic['username'] = ssh_user				#empty - load defaults
          if gatedic['password'] == None:	gatedic['password'] = ssh_pass
          if gatedic['port']     == None:	gatedic['port']     = ssh_port

          gatelist.append(gatedic)


        except KeyError: 
          print 'ERROR! Missing host declaration in '+sname

    except KeyError:
        pass

#resulttest
#  print 'Found '+str(len(netlist))+' networks'
#  for netx in netlist:
#    print netx
#
#  print 'Found '+str(len(hostslist))+' hosts:'
#  for hostm in hostslist:
#    print (hostm)
#
#  print 'Found '+str(len(gatelist))+' gatways'
#  for gatem in gatelist:
#    print (gatem)#
#
  print ' Hello. I found '+str(len(sectionlist))+' sections in '+cfg_file+' file, '+str(len(netlist))+' networks, '+str(len(hostslist))+' hosts and '+str(len(gatelist))+' gatways.'
  return {'net':netlist,'host':hostslist,'gate':gatelist,'redir':redirport0}



def nccsnmpalarms(ip):                                                      # Procedure to list a nCC alarms using SNMPwalk command - linux, access to ncc snnp is required.
  inode='1.3.6.1.4.1.1773.3.1.3.1.8'                                        # snnp command to walk:
  xex='snmpwalk'                                                            # snmpwalk command
  xop=' -v1 -cprivate'                                                      # oprions for SNMPwalk
  args=shlex.split(xex+' '+ip+' '+xop+' '+inode)
  proc=subprocess.Popen(args, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
  out,err=proc.communicate()
  outln=out.split('\n')
  alarms=[]
  for line in outln:
    line=line.strip('\r\n')
    if len (line)>0:
      state=int(line.split('= INTEGER:')[1])
      alarms.append(state)
  return alarms


def nccwait(ip,count=600):
  count=int(count)
  passed=True
  wait=True
  while wait:
    time.sleep(1)
    alarms=nccsnmpalarms(ip)
    critical=0
    for alarm in alarms:
      if alarm == 6:                     # 6 = critical, 5 = major, 4 = minor 8 = disabled
        critical+=1
    if critical == 0: wait=False
    else:
      count-=1
      print count
      if count <= 0:
        passed=False
        wait=False


  if passed: print 'nCC '+ip+' have no critical alarms'
  if count <= 0: print 'I was waiting a long time'
  if not passed: print 'nCC '+ip+' still have critical alarms'
  return passed









